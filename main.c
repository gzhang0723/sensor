
/**June 1 2017 @Zhibin Zhang
 **nRF5x PCA10040 board test data 
 */ 


#include <stdbool.h> 
#include <stdint.h> 
#include "nrf_delay.h" /*timing function*/ 
#include "boards.h"  /*all board hw related definition*/ 

int main (void) {
	
   /*configure the board*/ 
   bsp_board_leds_init(); 
   
   /*main loop - toggle LED*/ 
   while (true) 
   {
       for (int i=0; i < LEDS_NUMBER; i++) 
       {
          bsp_board_led_invert(i); 
          nrf_delay_ms(500); 
       }
   }  
}


